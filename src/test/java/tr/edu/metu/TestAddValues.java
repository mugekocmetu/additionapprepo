package tr.edu.metu;

import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import tr.edu.metu.model.AddValues;

// Test ADDValues Class
public class TestAddValues {
	@Test
	public void testAddValuesAsInt(){ 
		// Addition of two positive integers
		int x = 10;
		int y = 1;
		assertEquals(11, AddValues.getInstance().addValues(x, y));
	}
	
	@Test
	public void testAddValuesAsString(){
		// Addition of two string values
		String x = "10";
		String y = "1";
		assertEquals(11, AddValues.getInstance().addValues(x, y));
	} 
	
	@Test  
	public void testAddValue0AsInt(){
		// Addition of an integer with 0
		int x = 10;
		int y = 0;
		assertEquals(10, AddValues.getInstance().addValues(x, y));
	}
	
	@Test 
	public void testAddValue0AsString(){
		// Addition of a string with 0
		String x = "10";
		String y = "0";
		assertEquals(10, AddValues.getInstance().addValues(x, y));
	}
	
	@Test
	public void testAddNegativeValueAsInt(){
		// Addition of an integer with negative value
		int x = -5;
		int y = 10;
		assertEquals(5, AddValues.getInstance().addValues(x, y));
	}
	
	@Test
	public void testAddNegativeValueAsString(){
		// Addition of a string with negative value
		String x = "-5";
		String y = "10";
		assertEquals(5, AddValues.getInstance().addValues(x, y));
	}
	@Test
	public void testAddNegativeValuesAsInt(){
		// Addition of two negative integers
		int x = -5;
		int y = -7;
		assertEquals(-12, AddValues.getInstance().addValues(x, y));
	}
	
	@Test
	public void testAddNegativeValuesAsString(){
		// Addition of two negative string values
		String x = "-5";
		String y = "-7";
		assertEquals(-12, AddValues.getInstance().addValues(x, y));
	}
	
	@Test
	public void testAddMoreValues(){
		// Addition of more than two integers with negative and positive values
		String x = "-50";
		String y = "-7";
		Integer total = AddValues.getInstance().addValues(x, y);
		x = "107";
		assertEquals(50, AddValues.getInstance().addValues(x, total.toString()));
	}
	
//	@Test
//	void test() {
//		fail("Not yet implemented");
//	}
}
