package tr.edu.metu.model;

import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;


public class AddValues implements RequestHandler<Map<String, List<Integer>>, String> {
	
	private static AddValues instance;
	
	// Static method to create instance of AddValues class 
    public static AddValues getInstance() 
    { 
        if (instance == null) 
            instance = new AddValues(); 
  
        return instance; 
    } 

 	// AWS method
	@Override
	public String handleRequest(Map<String, List<Integer>> input, Context context) {
		List<Integer> list = input.get("numbers");
		Integer totalNumber = 0;
		for(int i:list) {
			totalNumber+=i;
		}
		return "Response is: " + totalNumber; 
	}
	
	//Covert strings as integers and add their values
	public Integer addValues(String i1, String i2) {	
		Integer sum = null;
		if(i1 != null && i2 != null) {
			try{
				int i1Int= Integer.parseInt(i1);
				int i2Int = Integer.parseInt(i2);
				sum = addValues(i1Int, i2Int);
			} catch(NumberFormatException e1) {
				// Write the error to the console
				System.out.println(e1.getMessage());
			}
		} else {
			System.out.println("Values are null");
		}
		return sum; 
	}
	
	// Add to integers values
	public Integer addValues(int i1, int i2) {
		int sum = i1 + i2;
		return sum;
	}

}
