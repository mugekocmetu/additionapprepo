package tr.edu.metu.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import tr.edu.metu.model.AddValues;

public class AddValuesView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6813230047794097845L;

	/**
	 * Launch the application.
	 */
	public AddValuesView() {
		JFrame frame = new JFrame();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JPanel pnlAddValues = new JPanel();
		pnlAddValues.setLayout(null);

		// Create a label to give information to the user
		JLabel lblProductId = new JLabel("Please write the numbers you want to add");
		lblProductId.setBounds(17, 32, 288, 16);
		pnlAddValues.add(lblProductId);
		
		// Create number field for user to write values
		JTextField txtNumber = new JTextField();
		txtNumber.setBounds(17, 60, 130, 26);
		pnlAddValues.add(txtNumber);
		txtNumber.setColumns(10);	
		
		// Create total label
		JLabel lblTotalValueString = new JLabel("Total is: ");
		lblTotalValueString.setBounds(27, 95, 107, 16);
		pnlAddValues.add(lblTotalValueString);
		
		// Create sum label
		JLabel lblSum = new JLabel("0");
		lblSum.setBounds(85, 95, 294, 16);
		pnlAddValues.add(lblSum);
		
		// Create add button. 
		// User enters the button when he/she wants to add values
		JButton btnAddNumberButton = new JButton("Add");
		btnAddNumberButton.setBounds(150, 59, 117, 29);
		pnlAddValues.add(btnAddNumberButton);

		// If user clicks add button, sum label with user's entered value and change sum label with new total
		btnAddNumberButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Integer sum = AddValues.getInstance().addValues(lblSum.getText(), txtNumber.getText());
				if(sum != null) {
					lblSum.setText(sum.toString());
				} else {
					JOptionPane.showMessageDialog(null,
							"Please enter a number correctly!");
				}
				// Clear the number text
				txtNumber.setText("");
			}
		});
		
		System.out.println("muge");
		pnlAddValues.setVisible(true);
		frame.getContentPane().add(pnlAddValues);
		frame.setVisible(true);
		frame.setTitle("Addition System");
	}
	
}
